<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Controller;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/eurofxref_daily',[Controller::class, 'eurofxref_daily']);
Route::get('/gethistory',[Controller::class, 'getHistory']);
Route::match(['get','post'],'/add_to_history',[Controller::class, 'add_to_history']);

Route::fallback(function(){
    return response()->json(['message' => 'Page Not Found'], 404);
});

    
    

    