<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class History extends Model
{
    use HasFactory;
    protected $table = 'calc_history';
    //protected $primaryKey = 'id';
    //public $incrementing = true;
    //protected $keyType = 'int';
}
