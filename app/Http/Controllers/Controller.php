<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Validator;
use PHPUnit\Util\Json;
use App\Models\History;

class Controller extends BaseController
{
    
    /**
     * Get currency to euro.
     *
     * @return Json
     */
    public function eurofxref_daily()
    {
        $response = Http::get(config('app.eurofxref_daily_url'));
        
        $xml = simplexml_load_string($response->body());
        
        $data = $xml->Cube->Cube ?? null;
        $arrayJson = [];
        $arrayJson['date'] = (string)$data->attributes()->time;
        $arrayJson['currencies'] = [];
        
        foreach($data->Cube as $e) {
            $arrayJson['currencies'][] = 
                [
                    "currency" =>(string)$e->attributes()->currency,
                    "rate"     =>(string)$e->attributes()->rate
                ];
        }
        $arrayJson['currencies'][] = ["currency"=>"EUR","rate"=>1];
        return response($arrayJson, 200)->header('Access-Control-Allow-Origin', '*')
            ->header('Content-Type', 'application/json');
    }
    
    /**
     * Save data to history
     */
    public function add_to_history(Request $request) {
        $post = $request->json()->all();
        
        try {
            $validator = Validator::make($post, [
                'sourceCurrency'=>'required|alpha',
                'sourceCurrencyVal'=>'required|numeric',
                'amount'=>'required|numeric',
                'destinationCurrency'=>'required|alpha',
                'destinationCurrencyVal'=>'required|numeric',
                'calculationResult'=>'required|numeric',
                
                'note' => 'required|min:|max:90'
            ]);
            
            if ($validator->passes()) {
                $history = new History();
                $history->SourceCurrency = $post['sourceCurrency'];
                $history->SourceRate = $post['sourceCurrencyVal'];
                $history->SourceAmount = $post['amount'];
                $history->DestinationCurrency = $post['destinationCurrency'];
                $history->DestinationRate = $post['destinationCurrencyVal'];
                $history->CalculatedAmount = $post['calculationResult'];
                $history->Note = $post['note'];
                
                $history->save();
            } else {
                //print_r($validator->getMessageBag());
                throw new \Exception("Form validation failed");
            }

            
        }catch (\Exception $e) {
            return response($e->getMessage(), 500)->header('Access-Control-Allow-Origin', '*')
            ->header('Content-Type', 'application/json');
        }

        return response("success", 200)->header('Access-Control-Allow-Origin', '*')
            ->header('Content-Type', 'application/json');
    }

    public function getHistory() {
        $rows = History::query()->where('created_at', '>', DB::raw('NOW() - INTERVAL 30 DAY'))->orderBy('created_at', 'desc')->get();
            
        return response($rows, 200)->header('Access-Control-Allow-Origin', '*')
        ->header('Content-Type', 'application/json');
    }
}
